
import java.io.IOException;
import java.sql.*;                              // Enable SQL processing
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.Scanner;

public class JDBC
{
	static Scanner input = new Scanner(System.in);
	static Connection connection; 

	public static void main(String[] arg) throws Exception
	{

		// Incorporate mySQL driver
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		credentialInput();
		System.out.println(""); 

	}

	private static void menuScreen() throws SQLException, IOException {
		System.out.println(""); 
		System.out.println("MAIN MENU:"); 
		System.out.println("1 Find movies featuring a specified star");
		System.out.println("2 Insert a star or customer into the database");
		System.out.println("3 Delete a customer from the database");
		System.out.println("4 Print: name of each table and their attirutes and type");
		System.out.println("5 Enter a SQL command");
		System.out.println("6 Add movie to database");
		System.out.println("7 Exit menu");
		System.out.println("8 Quit");

		System.out.println(""); 
		System.out.print("Please enter the number that corresponds to your selection: "); 
		int userSelect = input.nextInt();

		processMenuOption(userSelect);
	}

	private static void processMenuOption(int userSelection) throws SQLException, IOException {
		switch(userSelection){
		case 1: findMovieByStar(); 
		break;
		case 2:
			System.out.println("1. Insert a new star into database");
			System.out.println("2. Insert a new customer into database"); 
			System.out.print("Enter the number that corresponds to your selection: ");
			int select = input.nextInt(); 
			if (select == 1){
				insertNewStar(connection);
			}
			else if(select == 2){
				insertNewCustomer(connection);
			}
			else {
				invalidSelectionMsg();  
			}
			break;
		case 3: deleteCustomer(); 
		break;
		case 4: 
			printMetaData();
			break; 
		case 5: System.out.println("What SQL Would You Like to Do?"); 
		System.out.println("1. SELECT");
		System.out.println("2. UPDATE");
		System.out.println("3. INSERT");
		System.out.println("4. DELETE");
		int command = input.nextInt();
		sqlCommandHelper(command); 
		break; 
		case 6: addMovie(); 
		break; 
		case 7: exitOption(); 
		break; 
		case 8: exitProgram(); 
		break; 
		default: invalidSelectionMsg();
		menuScreen(); 
		break; 
		}

	}

	private static void addMovie() throws IOException {
		//		System.out.println("Enter id");
		//		int id = input.nextInt();
		input.nextLine();
		System.out.println("Enter title:");
		String title = input.nextLine();

		String query = "SELECT title FROM movies WHERE title LIKE '%" +
				title + "%'";
		System.out.println("Checking if movie exists in the databse..");
		Statement select;
		try {
			select = connection.createStatement();
			ResultSet result = select.executeQuery(query);
			if (result.isBeforeFirst()) {
				result.next();
				String test = result.getString(1);
				System.out.println(test);
				System.out.println("A movie with that title already exists in"
						+ " the database..");
				menuScreen();
				return;
			}

			System.out.println("Enter year:");
			int year = input.nextInt();
			input.nextLine();
			System.out.println("Enter director:");
			String director = input.nextLine();
			System.out.println("Enter banner_url:");
			String banner_url = input.nextLine();
			System.out.println("Enter trailer_url:");
			String trailer_url = input.nextLine();
			System.out.println("Enter a star's first name:");
			String firstName = input.nextLine();
			System.out.println("Enter a star's last name:");
			String lastName = input.nextLine();
			System.out.println("Enter a genre:");
			String genre = input.nextLine();

			int movieID = 0;

			System.out.println("Movie doesn't exist. Inserting movie into database..");
			query = "INSERT INTO movies (title, year, director, banner_url, "
					+ "trailer_url) VALUES (?, ?, ?, ?, ?)";
			PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, title);
			preparedStatement.setInt(2, year);
			preparedStatement.setString(3, director);
			preparedStatement.setString(4, banner_url);
			preparedStatement.setString(5, trailer_url);
			preparedStatement.executeUpdate();
			ResultSet rs = preparedStatement.getGeneratedKeys();
			if (rs.next()){
				movieID = rs.getInt(1);
			}

			int starID = 0;
			System.out.println("Checking if the star exists in the database..");
			query = "SELECT id FROM stars WHERE first_name = '" +
					firstName + "' AND last_name = '" + lastName + "'";
			Statement select2 = connection.createStatement();
			ResultSet result2 = select2.executeQuery(query);
			if (!result2.isBeforeFirst()) {
				System.out.println("Star doesn't exist. inserting star into the database");
				query = "INSERT INTO stars (first_name, last_name) " +
						"VALUES (?, ?)";
				PreparedStatement preparedStatement2 = connection.prepareStatement(query);
				preparedStatement2.setString(1, firstName);
				preparedStatement2.setString(2, lastName);
				preparedStatement2.executeUpdate();
			} else {
				result2.next();
				starID = result2.getInt(1);
			}

			int genreID = 0;
			System.out.println("Checking if the genre exists in the database..");
			query = "SELECT id FROM genres WHERE name = '" + 
					genre + "'";
			Statement select3 = connection.createStatement();
			ResultSet result3 = select3.executeQuery(query);
			if (!result3.isBeforeFirst()) {
				System.out.println("Genre doesn't exist. Inserting genre into the database..");
				query = "INSERT INTO genres (name) " +
						"VALUES (?)";
				PreparedStatement preparedStatement3 = connection.prepareStatement(query);
				preparedStatement3.setString(1, genre);
				preparedStatement3.executeUpdate();
			} else {
				result3.next();
				genreID = result3.getInt(1);
			}

			System.out.println("Linking star id and movie id..");
			query = "INSERT INTO stars_in_movies (star_id, movie_id) " +
					"VALUES (?, ?)";
			PreparedStatement preparedStatement4 = connection.prepareStatement(query);
			preparedStatement4.setInt(1, starID);
			preparedStatement4.setInt(2, movieID);
			preparedStatement4.executeUpdate();

			System.out.println("Linking genre id and movie id..");
			query = "INSERT INTO genres_in_movies (genre_id, movie_id) " +
					"VALUES (?, ?)";
			PreparedStatement preparedStatement2 = connection.prepareStatement(query);
			preparedStatement2.setInt(1, genreID);
			preparedStatement2.setInt(2, movieID);
			preparedStatement.executeUpdate();

			System.out.println("Successfully Inserted!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void exitProgram() throws SQLException, IOException {
		System.out.println("Are you sure you would like to terminated the program? "); 
		System.out.print("Enter 'y' for yes or 'n' for no. ");
		String in = input.next(); 
		if (in.equals("y")){
			connection.close();
			System.exit(0);
		}
		else if (in.equals("n")){
			menuScreen(); 
		}
		else {
			invalidSelectionMsg();
			System.out.println(); 
			exitProgram(); 
		}
	}

	private static void sqlCommandHelper(int command) throws SQLException {
		switch(command){
		case 1: selectRecord();
		break;
		case 2: updateRecord(); 
		break; 
		case 3: //NEED TO WRITE THIS
			insertRecord();
			break;
		case 4: deleteRecord();
		break;
		default: invalidSelectionMsg();
		break;   
		}
	}


	/*MAIN MENU FUNCTIONS */
	private static void findMovieByStar() throws SQLException {
		Scanner sc = createSystemIn();
		System.out.print("Enter the star's id, first name, or last name: ");
		String search = sc.nextLine();
		Statement select = connection.createStatement();
		ResultSet result = null;
		if (isInteger(search)) {
			int starid = Integer.parseInt(search);
			//             Statement select = connection.createStatement();
			result = select.executeQuery("SELECT m.id, m.title, m.year, m.director, m.banner_url, m.trailer_url FROM stars_in_movies AS sim, movies AS m WHERE sim.star_id = " + starid + " AND sim.movie_id = m.id");
			printMovieColumns(result);
			printMovieResults(result);
		} else if (search.contains(" ")) {
			int spaces = search.length() - search.replaceAll(" ", "").length();
			if (spaces >= 2) {
				System.out.println("Please search for the first and last name separately! (No middle names!)");
				findMovieByStar();
				return;
			}
			Scanner sc2 = new Scanner(search);
			String firstName = sc2.next();
			String lastName = sc2.next();
			if (sc2.hasNext()) {
				firstName = firstName + " " + lastName;
				lastName = sc2.next();
			}
			result = select.executeQuery("SELECT m.id, m.title, m.year, m.director, m.banner_url, m.trailer_url FROM movies AS m, stars AS s, stars_in_movies AS sim WHERE m.id = sim.movie_id AND s.id = sim.star_id AND s.first_name = '" + firstName + "' AND s.last_name = '" + lastName + "' UNION " +
					"SELECT m.id, m.title, m.year, m.director, m.banner_url, m.trailer_url FROM movies AS m, stars AS s, stars_in_movies AS sim WHERE m.id = sim.movie_id AND s.id = sim.star_id AND s.first_name = '" + firstName + " " + lastName + "'");
			printMovieColumns(result);
			printMovieResults(result);
		} else {
			result = select.executeQuery("SELECT m.id, m.title, m.year, m.director, m.banner_url, m.trailer_url FROM movies AS m, stars AS s, stars_in_movies AS sim WHERE m.id = sim.movie_id AND s.id = sim.star_id AND (s.first_name = '" + search + "' OR s.last_name = '" + search + "')");
			printMovieColumns(result);
			printMovieResults(result);
		}
	}

	private static void deleteCustomer() throws SQLException {
		Statement update = connection.createStatement();



		System.out.println("How would you like to delete the customer?"); 
		System.out.println("1. ID"); 
		System.out.println("2. First Name"); 
		System.out.println("3. Last Name");
		int delete = input.nextInt(); 

		String customerInput = "";
		int result = 0; 

		switch(delete){
		case 1: System.out.print("Customer ID To Delete: "); 
		customerInput = input.next();
		result = update.executeUpdate("DELETE FROM customers WHERE id = " + customerInput);
		System.out.println("ID " + customerInput + " was deleted successfuly from the customer table."); 

		break; 
		case 2: System.out.print("Customer First Name To Delete: "); 
		customerInput = input.next();
		result = update.executeUpdate("DELETE FROM customers WHERE first_name = '" + customerInput + "'");
		System.out.println(customerInput + " was deleted successfuly from the customer table."); 

		break; 
		case 3:System.out.print("Customer Last Name To Delete: "); 
		customerInput  = input.next();
		result = update.executeUpdate("DELETE FROM customers WHERE last_name = '" + customerInput + "'");
		System.out.println(customerInput + " was deleted successfuly from the customer table."); 

		break; 
		default: invalidSelectionMsg();
		deleteCustomer(); 
		break; 
		} 

	}

	private static void insertNewCustomer(Connection connection) {
		System.out.println("Enter the customer's first and last name or last name");
		Scanner sc = createSystemIn();
		String name = sc.nextLine();
		String firstName = "";
		String lastName = "";
		String ccardid = "";
		String address = "";
		String email = "";
		String password = "";

		String[] n = nameSplit(name);
		firstName = n[0];
		lastName = n[1];

		System.out.println("Enter the customer's credit card id:");
		ccardid = sc.nextLine();
		Statement select;
		try {
			select = connection.createStatement();
			ResultSet result = select.executeQuery("SELECT * FROM creditcards WHERE id = " + ccardid);
			if (!result.next()) {
				System.out.println("Invalid credit card! Please try again.");
				insertNewCustomer(connection);
				return;
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("Please enter the customer's address:");
		address = sc.nextLine();
		System.out.println("Please enter the customer's e-mail:");
		email = sc.nextLine();
		System.out.println("Please enter the customer's password:");
		password = sc.nextLine();
		String query = "INSERT INTO customers (first_name, last_name, cc_id, address, email, password) " +
				//              "VALUES ('" + firstName + "', '" + lastName + "', '" + dob + "', '" + photo_url + "')";
				"VALUES (?, ?, ?, ?, ?, ?)";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, firstName);
			preparedStatement.setString(2, lastName);
			preparedStatement.setString(3, ccardid);
			preparedStatement.setString(4, address);
			preparedStatement.setString(5, email);
			preparedStatement.setString(6, password);
			preparedStatement.executeUpdate();
			insertionSuccessful();
		} catch (SQLException e) {
			System.out.println();
		}
	}

	private static void insertNewStar(Connection connection) {
		System.out.print("Enter the star's full name or last name: ");
		Scanner sc = createSystemIn();
		String name = sc.nextLine();
		String firstName = "";
		String lastName = "";
		String dob = "";
		String photo_url = "";

		String[] n = nameSplit(name);
		firstName = n[0];
		lastName = n[1];

		System.out.print("Enter the star's date of birth (year-month-day e.g. 1990-10-30): ");
		dob = sc.nextLine();
		System.out.println("Enter star's photo_url (Optional; Press enter to skip)");
		photo_url = sc.nextLine();
		String query = "INSERT INTO stars (first_name, last_name, dob, photo_url) " +
				//              "VALUES ('" + firstName + "', '" + lastName + "', '" + dob + "', '" + photo_url + "')";
				"VALUES (?, ?, ?, ?)";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, firstName);
			preparedStatement.setString(2, lastName);
			preparedStatement.setString(3, dob);
			preparedStatement.setString(4, photo_url);
			preparedStatement.executeUpdate();
			insertionSuccessful();
		} catch (SQLException e) {
			invalidInputMsg();
		}
	}

	private static ResultSet executeStatement(Connection connection, String query) throws SQLException {
		Statement select = connection.createStatement();
		ResultSet result = select.executeQuery(query);
		return result;
	}

	private static void printCustomerTable(ResultSet result) throws SQLException {
		String id = result.getMetaData().getColumnName(1);
		String first_name = result.getMetaData().getColumnName(2);
		String last_name = result.getMetaData().getColumnName(3);
		String cc_id = result.getMetaData().getColumnName(4);
		String address = result.getMetaData().getColumnName(5);
		String email = result.getMetaData().getColumnName(6);
		String password = result.getMetaData().getColumnName(7);
		System.out.printf( "%-7s %-50s %-7s %-25s %-130s %-100s %n", id, first_name, last_name, cc_id, address, email, password);

	}
	private static void printMovieColumns(ResultSet result) throws SQLException {
		String id = result.getMetaData().getColumnName(1);
		String title = result.getMetaData().getColumnName(2);
		String year = result.getMetaData().getColumnName(3);
		String director = result.getMetaData().getColumnName(4);
		String banner_url = result.getMetaData().getColumnName(5);
		String trailer_url = result.getMetaData().getColumnName(6);
		System.out.printf( "%-7s %-50s %-7s %-25s %-130s %-100s %n", id, title, year, director, banner_url, trailer_url);
	}

	private static void printMovieResults(ResultSet result) {
		try {
			while (result.next()) {
				String movieID = result.getString("id");
				String movieName = result.getString("title");
				String movieYear = result.getString("year");
				String movieDirector = result.getString("director");
				String movieBanner = result.getString("banner_url");
				String movieTrailer = result.getString("trailer_url");
				System.out.printf( "%-7s %-50s %-7s %-25s %-130s %-100s %n", movieID, movieName, movieYear, movieDirector, movieBanner, movieTrailer);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void printMetaData() throws SQLException {
		String query = "show tables";
		ResultSet result = executeStatement(connection, query);
		while(result.next()) {
			String table = result.getString("Tables_in_moviedb");
			System.out.println(table);
			System.out.println("--------------------");
			query = "SELECT * FROM " + table + " limit 1";
			ResultSet r = executeStatement(connection, query);
			ResultSetMetaData metadata = r.getMetaData();
			while (r.next()) {
				for (int j = 1; j <= metadata.getColumnCount(); j++) {
					System.out.println(metadata.getColumnName(j) + ": " + metadata.getColumnTypeName(j));
				}
				System.out.println("");
			}
		}
	}

	private static void printTablesInMenu() {
		System.out.println("1. creditcards");
		System.out.println("2. customers");
		System.out.println("3. genres");
		System.out.println("4. genres_in_movies");
		System.out.println("5. movies");
		System.out.println("6. sales");
		System.out.println("7. stars");
		System.out.println("8. stars_in_movies");
	}

	/*SQL COMMANDS */
	private static void selectRecord () throws SQLException {

		Scanner sc = createSystemIn();
		// Statement update = connection.createStatement();
		PreparedStatement update;

		System.out.print("Please enter the column name you would like to select: ");
		String colName = sc.next(); 


		System.out.print("From which table would you like to select from? ");
		String tableName = sc.next(); 

		String query =  "SELECT " + colName + " FROM " + tableName; 
		update = connection.prepareStatement(query);
		ResultSet result = update.executeQuery();
		ResultSetMetaData metadata = result.getMetaData();

		System.out.println(""); 
		System.out.println(""); 
		System.out.println(colName + ":");
		while (result.next()) {
			if (metadata.getColumnTypeName(1).equals("INT")) {
				System.out.println(result.getInt(1));
			} else {
				System.out.println(result.getString(1));
			}
		}

	}


	private static void updateRecord() {
		printTablesInMenu();
		input.nextLine();

		System.out.println("Which table would you like to update? (Type the name)");     
		String table = input.nextLine();

		System.out.println("Which column would you like to modify?");
		String col = input.nextLine();

		System.out.println("What value would you like to replace this attribute with?");
		String newValue = input.nextLine();

		System.out.println("Which condition do you want to operate on? (>, <, =, <=, >=, !=)");
		String condition = input.nextLine();

		System.out.println("Which column should this condition apply to?");
		String col2 = input.nextLine();

		System.out.println("What is the value that the condition must satisfy?");
		String val = input.nextLine();

		String query = "UPDATE " + table + " SET " + col + " = ? WHERE " + col2 + " " + condition + " ?";  
		PreparedStatement update;
		try {
			update = connection.prepareStatement(query);
			if (isInteger(newValue)) {
				int v = Integer.parseInt(newValue);
				update.setInt(1, v);
			} else {
				update.setString(1, newValue);
			}
			if (isInteger(val)) {
				int v2 = Integer.parseInt(val);
				update.setInt(2, v2);
			} else {
				update.setString(2, val);
			}
			int i = update.executeUpdate();
			System.out.println(i + " records updated");
		} catch (SQLException e) {
			invalidInputMsg();
		}
	}

	private static void insertRecord() throws SQLException {
		printTablesInMenu();

		System.out.println("Which table would you like to insert into? (Type the name)");     
		String table = input.next();

		String query = "SELECT * FROM " + table;
		Statement select = connection.createStatement();
		ResultSet result = select.executeQuery(query);
		ResultSetMetaData metadata = result.getMetaData();

		ArrayList<String> values = new ArrayList<String>();
		boolean hasID = false;
		for (int i = 1; i <= metadata.getColumnCount(); i++) {
			if (metadata.getColumnName(i).equals("id") && !table.equals("creditcards")) {
				hasID = true;
				continue;
			}
			System.out.println("Please enter the " + metadata.getColumnName(i));
			String in = input.next();
			values.add(in);
		}
		query = "INSERT INTO " + table + " (";
		for (int k = 1; k <= metadata.getColumnCount(); k++) {
			if (hasID) {
				hasID = false;
				continue;
			}
			if (k == metadata.getColumnCount()) {
				query += metadata.getColumnName(k) + ")";
			} else {
				query += metadata.getColumnName(k) + ", ";
			}
		}
		query += " VALUES(";
		for (int p = 0; p < values.size(); p++) {
			if (p == values.size() - 1) {
				query += "?)";
			} else {
				query += "?, ";
			}
		}
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		for (int j = 0; j < values.size(); j++) {
			if (isInteger(values.get(j))) {
				int v = Integer.parseInt(values.get(j));
				preparedStatement.setInt(j + 1, v);
			} else if (values.get(j).contains("-")) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date parsed = null;
				try {
					parsed = format.parse(values.get(j));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				java.sql.Date sql = new java.sql.Date(parsed.getTime());
				preparedStatement.setDate(j + 1, sql);
			}
			else {
				preparedStatement.setString(j + 1, values.get(j));
			}
		}
		try {
			preparedStatement.executeUpdate();
			System.out.println("Successfully inserted!");
		} catch (SQLException e) {
			invalidInputMsg();
		}
	}

	private static void deleteRecord() throws SQLException {
		Scanner sc = createSystemIn();
		Statement update = connection.createStatement();
		//PreparedStatement update; 
		System.out.print("From which table would you like to delete from? ");
		String tableInput = sc.next();
		System.out.println();

		System.out.print("Which attribute would you like to delete? ");
		String attributeInput = sc.next();
		System.out.println(); 

		System.out.print("What value of attribute would you like to delete? "); 
		String valueInput = sc.next();

		// String query = "DELETE FROM " + tableInput + " WHERE " +  attributeInput +" = '" + valueInput +"'";  
		// update = connection.prepareStatement(query);
		//update.executeUpdate(query);

		update.executeUpdate("DELETE FROM " + tableInput + " WHERE " + attributeInput + " = '" + valueInput + "'");
		System.out.println(valueInput + " has been deleted from " + tableInput + " table.");

	}

	private static void exitOption() throws IOException, SQLException {
		Scanner sc = createSystemIn();
		System.out.println("Would you like to exit menu?");
		System.out.print("Press 'y' for yes or 'n' for no. "); 
		String answer = sc.next();
		if (answer.equals("y")) {
			connection.close();
			credentialInput();
		}
		else {
			menuScreen();
		}
	}

	// Separates first and last name
	private static String[] nameSplit(String name) {
		String firstName = "";
		String lastName = "";
		if (name.contains(" ")) {
			Scanner nameScanner = new Scanner(name);
			firstName = nameScanner.next();
			lastName = nameScanner.next();
		} else {
			firstName = "";
			lastName = name;
		}
		String[] n = new String[2];
		n[0] = firstName;
		n[1] = lastName;
		return n;
	}

	private static boolean isInteger(String s) {
		try { 
			Integer.parseInt(s); 
		} catch(NumberFormatException e) { 
			return false; 
		}
		return true;
	}

	private static Scanner createSystemIn() {
		Scanner sc = new Scanner(System.in);
		return sc;
	}

	private static void credentialInput() throws IOException, SQLException {
		// Ask user for user name and password
		System.out.print("Username: ");
		String username = input.nextLine();
		System.out.print("Password: "); 
		String pw = input.nextLine();
		accessValidator(username, pw);
	}

	private static void accessValidator(String user, String pw) throws IOException, SQLException {
		// if login is valid, access granted message show and menu screen appear 
		try{
			// Connect to the test database
			connection = DriverManager.getConnection("jdbc:mysql:///moviedb", user, pw);
			accessGrantedMessage();
		}
		//if not valid, access not allowed message shows and explains why
		//allow user to exit 
		catch (Exception e) {
			accessDeniedMessage(); 
			deniedReason();
			System.out.print("Would you like to try again? Press 'y' for yes and 'n' for no'. "); 
			String answer = input.next(); 
			if (answer.equals("y")) {
				credentialInput();
			}
			else if (answer.equals("n")){
				System.out.print("Program will now terminate.");
				System.exit(0);
			}
			else {
				System.out.println("Invalid Selection."); 
				System.out.println();
				accessValidator(user, pw);
			}

		}
		while (true)
			menuScreen();
	}


	/*MESSAGES*/
	private static void accessGrantedMessage() {
		System.out.println("Access Granted!");
	}
	private static void accessDeniedMessage() {
		System.out.println("Access Denied!"); 
	}  
	private static void deniedReason() {
		System.out.println("Incorrect Password.");
	}
	private static void invalidSelectionMsg() {
		System.out.println("Invalid selection. Please try again");
	}
	private static void invalidInputMsg() {
		System.out.println("Insertion failed! Invalid input?");
	}
	private static void insertionSuccessful() {
		System.out.println("Insertion successful!");
	}

}