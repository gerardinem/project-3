
import java.io.IOException;
import java.sql.*;                              // Enable SQL processing
import java.util.Scanner;

public class JDBC1
{
       static Scanner input = new Scanner(System.in);
       static Connection connection; 
    
       public static void main(String[] arg) throws Exception
       {

               // Incorporate mySQL driver
               Class.forName("com.mysql.jdbc.Driver").newInstance();

//               Connection connection = accessValidator();
               credentialInput();
               System.out.println(""); 
//               printMenu();
//               selectOption(connection);
//               processOption(1, connection);
                 //insertNewCustomer(connection);
               

               // Create an execute an SQL statement to select all of table"Stars" records
//               Statement select = connection.createStatement();
//               ResultSet result = select.executeQuery("Select * from stars");
//
//               // Get metatdata from stars; print # of attributes in table
//               System.out.println("The results of the query");
//               ResultSetMetaData metadata = result.getMetaData();
//               System.out.println("There are " + metadata.getColumnCount() + " columns");
//
//               // Print type of each attribute
//               for (int i = 1; i <= metadata.getColumnCount(); i++)
//                       System.out.println("Type of column "+ i + " is " + metadata.getColumnTypeName(i));
//
//               // print table's contents, field by field
//               while (result.next())
//               {
//                       System.out.println("Id = " + result.getInt(1));
//                       System.out.println("Name = " + result.getString(2) + result.getString(3));
//                       System.out.println("DOB = " + result.getString(4));
//                       System.out.println("photoURL = " + result.getString(5));
//                       System.out.println();
//               }
       }
       
       private static void menuScreen() throws SQLException, IOException {
           System.out.println(""); 
           System.out.println("MAIN MENU:"); 
           System.out.println("1 Find movies featuring a specified star");
           System.out.println("2 Insert a star or customer into the database");
           System.out.println("3 Delete a customer from the database");
           System.out.println("4 Print: name of each table and their attirutes and type");
           System.out.println("5 Enter a SQL command");
           System.out.println("6 Exit menu");
           System.out.println("7 Quit");
           
           System.out.println(""); 
           System.out.println("Please enter the number that corresponds to your selection. "); 
           int userSelect = input.nextInt();
           
           processMenuOption(userSelect); 
       }
       
   private static void processMenuOption(int userSelection) throws SQLException, IOException {
       switch(userSelection){
            case 1: findMovieByStar(); 
                break;
            case 2:
                    System.out.println("1. Insert a new star into database");
                    System.out.println("2. Insert a new customer into database"); 
                    System.out.print("Enter the number that corresponds to your selection: ");
                    int select = input.nextInt(); 
                    if (select == 1){
                        insertNewStar(connection);
                    }
                    else if(select == 2){
                        insertNewCustomer(connection);
                    }
                    else {
                        invalidSelectionMsg();  
                    }
                    break;
            case 3: deleteCustomer(connection); 
                break;
            case 4: 
                    printMetaData();
                break; 
            case 5: System.out.println("What SQL Would You Like to Do?"); 
                    System.out.println("1. SELECT");
                    System.out.println("2. UPDATE");
                    System.out.println("3. INSERT");
                    System.out.println("4. DELETE");
                    int command = input.nextInt();
                    sqlCommandHelper(command); 
                break; 
            case 6: exitOption(); 
                break; 
            case 7:
                break; 
            default: invalidSelectionMsg();
                     menuScreen(); 
                break; 
       }
    
   }
       
   private static void sqlCommandHelper(int command) throws SQLException {
       switch(command){
            case 1: selectRecord(connection);
                break;
            case 2: updateRecord(); 
                break; 
            case 3: //NEED TO WRITE THIS
                    insertRecord();
                break;
            case 4: deleteRecord(connection);
                break;
            default: invalidSelectionMsg();
               break;   
       }
}


/*MAIN MENU FUNCTIONS */
private static void findMovieByStar() throws SQLException {
       Scanner sc = createSystemIn();
       System.out.println("Enter the star's id, first name, or last name");
       String search = sc.nextLine();
       Statement select = connection.createStatement();
       ResultSet result = null;
       if (isInteger(search)) {
           int starid = Integer.parseInt(search);
//             Statement select = connection.createStatement();
           result = select.executeQuery("SELECT m.id, m.title, m.year, m.director, m.banner_url, m.trailer_url FROM stars_in_movies AS sim, movies AS m WHERE sim.star_id = " + starid + " AND sim.movie_id = m.id");
           printMovieColumns(result);
           printMovieResults(result);
       } else if (search.contains(" ")) {
    	   int spaces = search.length() - search.replaceAll(" ", "").length();
    	   if (spaces >= 2) {
    		   System.out.println("Please search for the first and last name separately! (No middle names!)");
    		   findMovieByStar();
    		   return;
    	   }
           Scanner sc2 = new Scanner(search);
           String firstName = sc2.next();
           String lastName = sc2.next();
           if (sc2.hasNext()) {
        	   firstName = firstName + " " + lastName;
        	   lastName = sc2.next();
           }
           result = select.executeQuery("SELECT m.id, m.title, m.year, m.director, m.banner_url, m.trailer_url FROM movies AS m, stars AS s, stars_in_movies AS sim WHERE m.id = sim.movie_id AND s.id = sim.star_id AND s.first_name = '" + firstName + "' AND s.last_name = '" + lastName + "' UNION " +
        		   "SELECT m.id, m.title, m.year, m.director, m.banner_url, m.trailer_url FROM movies AS m, stars AS s, stars_in_movies AS sim WHERE m.id = sim.movie_id AND s.id = sim.star_id AND s.first_name = '" + firstName + " " + lastName + "'");
           printMovieColumns(result);
           printMovieResults(result);
       } else {
           result = select.executeQuery("SELECT m.id, m.title, m.year, m.director, m.banner_url, m.trailer_url FROM movies AS m, stars AS s, stars_in_movies AS sim WHERE m.id = sim.movie_id AND s.id = sim.star_id AND (s.first_name = '" + search + "' OR s.last_name = '" + search + "')");
           printMovieColumns(result);
           printMovieResults(result);
         }
   }

    private static void deleteCustomer(Connection connection) throws SQLException {
        Statement update = connection.createStatement();
           
        System.out.println("How would you like to delete the customer?"); 
        System.out.println("1. ID"); 
        System.out.println("2. First Name"); 
        System.out.println("3. Last Name");
        int delete = input.nextInt(); 
        
         String customerInput = "";
        switch(delete){
            case 1: System.out.print("Customer ID To Delete: "); 
                customerInput = input.next();
                update.executeUpdate("DELETE FROM CUSTOMERS"  + "WHERE id" +  " = " + customerInput); 
                break; 
            case 2: System.out.print("Customer First Name To Delete: "); 
                customerInput  = input.next();
                update.executeUpdate("DELETE FROM CUSTOMERS"  + "WHERE first_name" +  " = " + customerInput); 
                break; 
            case 3:System.out.print("Customer ID To Delete: "); 
                customerInput  = input.next();
                update.executeUpdate("DELETE FROM CUSTOMERS"  + "WHERE last_name" +  " = " + customerInput); 
                break; 
            default: invalidSelectionMsg();
                     deleteCustomer(connection); 
                break; 
        }
      
         
         update.executeUpdate("DELETE FROM CUSTOMERS"  + "WHERE id" +  " = " + customerInput); 
    }

    private static void insertNewCustomer(Connection connection) {
           System.out.println("Enter the customer's first and last name or last name");
           Scanner sc = createSystemIn();
           String name = sc.nextLine();
           String firstName = "";
           String lastName = "";
           String ccardid = "";
           String address = "";
           String email = "";
           String password = "";
           
           String[] n = nameSplit(name);
           firstName = n[0];
           lastName = n[1];
           
           System.out.println("Enter the customer's credit card id:");
           ccardid = sc.nextLine();
           Statement select;
           try {
        	   select = connection.createStatement();
        	   ResultSet result = select.executeQuery("SELECT * FROM creditcards WHERE id = " + ccardid);
        	   if (!result.next()) {
        		   System.out.println("Invalid credit card! Please try again.");
        		   insertNewCustomer(connection);
        		   return;
        	   }
           } catch (SQLException e1) {
        	   // TODO Auto-generated catch block
        	   e1.printStackTrace();
           }
           System.out.println("Please enter the customer's address:");
           address = sc.nextLine();
           System.out.println("Please enter the customer's e-mail:");
           email = sc.nextLine();
           System.out.println("Please enter the customer's password:");
           password = sc.nextLine();
           String query = "INSERT INTO customers (first_name, last_name, cc_id, address, email, password) " +
//              "VALUES ('" + firstName + "', '" + lastName + "', '" + dob + "', '" + photo_url + "')";
                   "VALUES (?, ?, ?, ?, ?, ?)";
           try {
               PreparedStatement preparedStatement = connection.prepareStatement(query);
               preparedStatement.setString(1, firstName);
               preparedStatement.setString(2, lastName);
               preparedStatement.setString(3, ccardid);
               preparedStatement.setString(4, address);
               preparedStatement.setString(5, email);
               preparedStatement.setString(6, password);
        	   preparedStatement.executeUpdate();
        	   insertionSuccessful();
           } catch (SQLException e) {
        	   System.out.println();
           }
       }
       
       private static void insertNewStar(Connection connection) {
           System.out.println("Enter the star's full name or last name");
           Scanner sc = createSystemIn();
           String name = sc.nextLine();
           String firstName = "";
           String lastName = "";
           String dob = "";
           String photo_url = "";
           
           String[] n = nameSplit(name);
           firstName = n[0];
           lastName = n[1];
           
           System.out.println("Enter the star's date of birth (year-month-day e.g. 1990-10-30)");
           dob = sc.nextLine();
           System.out.println("Enter star's photo_url (Optional; Press enter to skip)");
           photo_url = sc.nextLine();
           String query = "INSERT INTO stars (first_name, last_name, dob, photo_url) " +
//              "VALUES ('" + firstName + "', '" + lastName + "', '" + dob + "', '" + photo_url + "')";
                   "VALUES (?, ?, ?, ?)";
           try {
        	   PreparedStatement preparedStatement = connection.prepareStatement(query);
        	   preparedStatement.setString(1, firstName);
        	   preparedStatement.setString(2, lastName);
        	   preparedStatement.setString(3, dob);
        	   preparedStatement.setString(4, photo_url);
        	   preparedStatement.executeUpdate();
        	   insertionSuccessful();
           } catch (SQLException e) {
        	   invalidInputMsg();
           }
       }
       
       private static ResultSet executeStatement(Connection connection, String query) throws SQLException {
           Statement select = connection.createStatement();
           ResultSet result = select.executeQuery(query);
           return result;
       }
       
       private static void printMovieColumns(ResultSet result) throws SQLException {
            String id = result.getMetaData().getColumnName(1);
            String title = result.getMetaData().getColumnName(2);
            String year = result.getMetaData().getColumnName(3);
            String director = result.getMetaData().getColumnName(4);
            String banner_url = result.getMetaData().getColumnName(5);
            String trailer_url = result.getMetaData().getColumnName(6);
            System.out.printf( "%-7s %-50s %-7s %-25s %-130s %-100s %n", id, title, year, director, banner_url, trailer_url);
       }
       
       private static void printMovieResults(ResultSet result) {
            try {
                while (result.next()) {
                    String movieID = result.getString("id");
                    String movieName = result.getString("title");
                    String movieYear = result.getString("year");
                    String movieDirector = result.getString("director");
                    String movieBanner = result.getString("banner_url");
                    String movieTrailer = result.getString("trailer_url");
                    System.out.printf( "%-7s %-50s %-7s %-25s %-130s %-100s %n", movieID, movieName, movieYear, movieDirector, movieBanner, movieTrailer);
                }
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
       }
       
      private static void printMetaData() throws SQLException {
           String query = "show tables";
           ResultSet result = executeStatement(connection, query);
           while(result.next()) {
               String table = result.getString("Tables_in_moviedb");
               System.out.println(table);
               System.out.println("--------------------");
               query = "SELECT * FROM " + table + " limit 1";
               ResultSet r = executeStatement(connection, query);
               ResultSetMetaData metadata = r.getMetaData();
               while (r.next()) {
                   for (int j = 1; j <= metadata.getColumnCount(); j++) {
                       System.out.println(metadata.getColumnName(j) + ": " + metadata.getColumnTypeName(j));
                   }
                   System.out.println("");
               }
           }
       }
      
      private static void printTablesInMenu() {
          System.out.println("1. creditcards");
          System.out.println("2. customers");
          System.out.println("3. genres");
          System.out.println("4. genres_in_movies");
          System.out.println("5. movies");
          System.out.println("6. sales");
          System.out.println("7. stars");
          System.out.println("8. stars_in_movies");
      }
       
       /*SQL COMMANDS */
       private static void selectRecord(Connection connection) throws SQLException {
           Scanner sc = createSystemIn();
           Statement update = connection.createStatement();
           System.out.print("How many columns would you like to SELECT from?");
           int numCol = sc.nextInt(); 
           System.out.println();
           
           System.out.println("Please Enter The Column Names:");
           String colName = ""; 
           for (int i = 0; i <= numCol; i++)
           {
               System.out.println("Please Enter Column Name # " + numCol+1); 
               if (colName.isEmpty())
               {
                   colName = sc.next(); 
               }
               else
               {
                   colName = colName + ", " + sc.next(); 
               }
           }
           
           System.out.print("From Which Table would you like to select from?");
           String tableName = sc.next(); 
           
           update.executeUpdate("SELECT" + colName + "FROM" + tableName); 
       }
       
       private static void updateRecord() {
    	   	printTablesInMenu();
    	   	
            System.out.println("Which table would you like to update? (Type the name)");     
            String table = input.nextLine();
            
            System.out.println("Which column would you like to modify?");
            String col = input.nextLine();
            
            System.out.println("What value would you like to replace this attribute with?");
            String newValue = input.nextLine();
            
            System.out.println("Which condition do you want to operate on? (>, <, =, <=, >=, !=)");
            String condition = input.nextLine();
            
            System.out.println("Which column should this condition apply to?");
            String col2 = input.nextLine();
            
            System.out.println("What is the value that the condition must satisfy?");
            String val = input.nextLine();
            
            String query = "UPDATE " + table + " SET " + col + " = ? WHERE " + col2 + " " + condition + " ?";  
            PreparedStatement update;
			try {
				update = connection.prepareStatement(query);
	            if (isInteger(newValue)) {
	            	int v = Integer.parseInt(newValue);
	            	update.setInt(1, v);
	            } else {
	            	update.setString(1, newValue);
	            }
	            if (isInteger(val)) {
	            	int v2 = Integer.parseInt(val);
	            	update.setInt(2, v2);
	            } else {
	            	update.setString(2, val);
	            }
	            update.executeUpdate();
			} catch (SQLException e) {
				invalidInputMsg();
			}
        }
       
        private static void insertRecord() {
            // TODO Auto-generated method stub
            
        }
       
       private static void deleteRecord(Connection connection) throws SQLException {
           Scanner sc = createSystemIn();
           Statement update = connection.createStatement();
           System.out.print("From which table would you like to delete from?");
           String tableInput = sc.next();
           System.out.println();
           
           System.out.print("Which attribute would you like to delete?");
           String attributeInput = sc.next();
           System.out.println(); 
           
           System.out.println("What value of attribute would you like to delete?"); 
           String valueInput = sc.next();
           
           update.executeUpdate("DELETE FROM" + tableInput + "WHERE" +  attributeInput +" = " + valueInput); 
       }
       
       private static void exitOption() throws IOException, SQLException {
           Scanner sc = createSystemIn();
           System.out.println("Would you like to exit?");
           System.out.print("Press 'y' for yes or 'n' for no. "); 
           String answer = sc.next();
           if (answer.equals("y")) {
               System.exit(0);
           }
           else {
        	   connection.close();
               credentialInput();
           }
       }

       // Separates first and last name
       private static String[] nameSplit(String name) {
           String firstName = "";
           String lastName = "";
           if (name.contains(" ")) {
               Scanner nameScanner = new Scanner(name);
               firstName = nameScanner.next();
               lastName = nameScanner.next();
           } else {
               firstName = "";
               lastName = name;
           }
           String[] n = new String[2];
           n[0] = firstName;
           n[1] = lastName;
           return n;
       }
       
       private static boolean isInteger(String s) {
            try { 
                Integer.parseInt(s); 
            } catch(NumberFormatException e) { 
                return false; 
            }
            return true;
        }
       
       private static Scanner createSystemIn() {
           Scanner sc = new Scanner(System.in);
           return sc;
       }
       
       private static void credentialInput() throws IOException, SQLException {
           // Ask user for user name and password
           System.out.println("Username:");
           String username = input.nextLine();
           System.out.print("Password: "); 
           String pw = input.nextLine();
           accessValidator(username, pw);
       }
       
       private static void accessValidator(String user, String pw) throws IOException, SQLException {
          // if login is valid, access granted message show and menu screen appear 
          try{
           // Connect to the test database
              connection = DriverManager.getConnection("jdbc:mysql:///moviedb", user, pw);
              accessGrantedMessage();
          }
          //if not valid, access not allowed message shows and explains why
               //allow user to exit 
           catch (Exception e) {
               accessDeniedMessage(); 
               deniedReason(); 
               exitOption(); 
           }
          while (true) {
        	  menuScreen();
          }
       }
       
       /*MESSAGES*/
       private static void accessGrantedMessage() {
           System.out.println("Access Granted!");
       }
       private static void accessDeniedMessage() {
           System.out.println("Access Denied!"); 
       }  
       private static void deniedReason() {
           System.out.println("Incorrect Password.");
       }
       private static void invalidSelectionMsg() {
               System.out.println("Invalid selection. Please try again");
        }
       private static void invalidInputMsg() {
    	   System.out.println("Insertion failed! Invalid input?");
       }
       private static void insertionSuccessful() {
    	   System.out.println("Insertion successful!");
       }

}